package com.usermanagement.app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserDAO {

	public UserDAO() {
		// TODO Auto-generated constructor stub
	}
	public void save(Connection con, User user) throws SQLException {
		if(con != null && !con.isClosed()) {
		   String str = "insert into users (first_name,last_name,email) value (?,?,?)";
		   PreparedStatement statement = con.prepareStatement(str);
		   statement.setString(1, user.getFirstName());
		   statement.setString(2, user.getLastName());
		   statement.setString(3, user.getEmail());
		   statement.executeUpdate();
		}
	}
	
	public void update(Connection con, User user) throws SQLException {
		if(con != null && !con.isClosed()) {
			   String str = "update users set first_name=?, last_name=?, email=? where user_id = ?";
			   PreparedStatement statement = con.prepareStatement(str);
			   statement.setString(1, user.getFirstName());
			   statement.setString(2, user.getLastName());
			   statement.setString(3, user.getEmail());
			   statement.setInt(4, user.getUserId());
			   statement.executeUpdate();
		}
	}
	
	public User get(Connection con, int userId) throws SQLException {
		if(con != null && !con.isClosed()) {
			   String str = "select * from users where user_id=?";
			   PreparedStatement statement = con.prepareStatement(str);
			   statement.setInt(1, userId);
			   ResultSet rs = statement.executeQuery();
			   rs.next();
			   User user = new User();
			   user.setUserId(rs.getInt("user_id"));
			   user.setFirstName(rs.getString("first_name"));
			   user.setLastName(rs.getString("last_name"));
			   user.setEmail(rs.getString("email"));
			   return user;
		}
		return null;
	}
	
	public void delete(Connection con, int userId) throws SQLException {
		if(con != null && !con.isClosed()) {
			   String str = "delete from users where user_id=?";
			   PreparedStatement statement = con.prepareStatement(str);
			   statement.setInt(1, userId);
			   statement.executeUpdate();
		}
	}

}
