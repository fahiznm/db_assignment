package com.usermanagement.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class UserManagementApp {

	private static void printMenu() {
		System.out.println("\n\n!!!! Welcome to User CRUD Services !!!!\n");
		System.out.println("..............................................\n");
		System.out.println("Enter the operation that you want to perform\n");
		System.out.println("1. Registration");
		System.out.println("2. Update");
		System.out.println("3. Display data");
		System.out.println("4. Delete");
		System.out.println("0. Exit");
		System.out.println("\n.............................................\n");
	}
    
	public static void main(String[] args) {
		try {
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_db","root", "admin");
			Statement stmt = connection.createStatement();
			Scanner scanner = new Scanner(System.in);
			UserDAO userDAO = new UserDAO();
			int input; 
			do {
				printMenu();
				input = Integer.parseInt(scanner.nextLine());
				switch(input) {
				  case 1:
					  User newUser = new User();
					  System.out.println("Enter first name:");
					  newUser.setFirstName(scanner.nextLine());
				      System.out.println("Enter last name:");
				      newUser.setLastName(scanner.nextLine());
				      System.out.println("Enter email:");
				      newUser.setEmail(scanner.nextLine());
				      userDAO.save(connection, newUser);
				      break;
				  case 2:
					  User user = new User();
					  System.out.println("Enter user id to update:");
					  user.setUserId(Integer.parseInt(scanner.nextLine()));
					  System.out.println("Enter first name:");
					  user.setFirstName(scanner.nextLine());
				      System.out.println("Enter last name:");
				      user.setLastName(scanner.nextLine());
				      System.out.println("Enter email:");
				      user.setEmail(scanner.nextLine());
				      userDAO.update(connection, user);
				      break;
				  case 3:
					  System.out.println("Enter user id to display:");
		              User user1 = userDAO.get(connection, Integer.parseInt(scanner.nextLine()));
		              System.out.println("firstName:" + user1.getFirstName() +" ,lastName:" + user1.getLastName() + " ,email:" + user1.getEmail());
		              break;
				  case 4:
					  System.out.println("Enter user id to delete:");
					  userDAO.delete(connection, Integer.parseInt(scanner.nextLine()));
					  System.out.println("Successfully deleted");
					  break;
				  default:
				}
				
			}while(input != 0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
