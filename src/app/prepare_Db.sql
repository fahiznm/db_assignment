create database user_db;
use user_db;
CREATE TABLE users (
    user_id int NOT NULL AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    last_name varchar(255),
    email varchar(255),
    primary key(user_id)
);